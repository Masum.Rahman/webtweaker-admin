from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm
from django.template.loader import render_to_string

from src.forms import ServiceOrderForm, AppointmentForm
from src.models import ServiceOrder


def save_appointment_form(request,form,template_name):
    data=dict()
    if request.method=='POST':
        if form.is_valid():
            form.save()
            print("form saved")
            data['form_is_valid']=True
        else:
            data['form_is_valid']=False
    context={'form':form}
    data['html_form']=render_to_string(template_name,context,request=request)
    return JsonResponse(data)

def appointment_create(request):
    if request.method=='POST':
        form=AppointmentForm(request.POST)
    else:
        form=AppointmentForm()

    return save_appointment_form(request,form,"src/partials/appointment.html")

def payments(request):
    print("kjashd")
    if request.method == 'POST':
        token=request.POST['token']
        print("Hello")
    return render(request,"src/payments.html",{})
#
# def list_service_orders(request):
#     return  render(request,"admin_site/")
#
# def service_order_list(request, template_name=''):
#     book = ServiceOrder.objects.all()
#     data = {}
#     data['object_list'] = book
#     return render(request, template_name, data)
#
# def servive_order_create(request, template_name='books_fbv/book_form.html'):
#     form = ServiceOrderForm(request.POST or None)
#     if form.is_valid():
#         form.save()
#         return redirect('books_fbv:book_list')
#     return render(request, template_name, {'form':form})
#
# def service_order_update(request, pk, template_name='books_fbv/book_form.html'):
#     book= get_object_or_404(ServiceOrder, pk=pk)
#     form = ServiceOrderForm(request.POST or None, instance=book)
#     if form.is_valid():
#         form.save()
#         return redirect('books_fbv:book_list')
#     return render(request, template_name, {'form':form})
#
# def service_order_delete(request, pk, template_name='books_fbv/book_confirm_delete.html'):
#     book= get_object_or_404(Book, pk=pk)
#     if request.method=='POST':
#         book.delete()
#         return redirect('books_fbv:book_list')
#     return render(request, template_name, {'object':book})
