from django.core.validators import MaxValueValidator, MinLengthValidator, MinValueValidator, RegexValidator
from django.db import models

# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver


class ServiceType(models.Model):
    name=models.CharField(max_length=200,null=False,blank=False)
    description=models.CharField(max_length=1000)

    def __str__(self):
        return self.name

class ServiceOrder(models.Model):

    APPOINTMENT_TYPES=(
        (1,'Request an Appointment'),
        (2,'Request a Quote')
    )

    name=models.ForeignKey(ServiceType,on_delete=models.CASCADE)
    completed=models.BooleanField(default=False)
    client_name=models.CharField(max_length=200,blank=False,null=False)
    client_mail=models.EmailField(max_length=30,blank=False,null=False)

    phone_regex=RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    client_phone=models.CharField(max_length=15,blank=False,null=False,validators=[phone_regex])


    car_year=models.IntegerField(validators=[MaxValueValidator(2018),
            MinValueValidator(1980)],null=True,blank=True)
    car_make=models.CharField(max_length=100)
    car_model=models.CharField(max_length=255)

    # appointment_type=models.PositiveSmallIntegerField(choices=APPOINTMENT_TYPES,default=1)
    appointment_date=models.DateField()
    message=models.TextField()

@receiver(post_save,sender=ServiceOrder)
def appointment_notification(sender,instance,created,**kwargs):
    if created:
        pass
    else:
        print("Hello from else signal")


class ContentManager(models.Model):
    pass

