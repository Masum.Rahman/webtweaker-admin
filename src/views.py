from django.shortcuts import render

# Create your views here.
from src.models import ServiceType


def home(request):
    services=ServiceType.objects.all()
    return render(request,"src/home.html",{'services':services})

def about_us(request):
    services=ServiceType.objects.all()
    return render(request,"src/about_us.html",{'services':services})

def services(request):
    services=ServiceType.objects.all()
    return render(request,"src/services.html",{'services':services})

def gallery(request):
    services=ServiceType.objects.all()
    return render(request,"src/gallery.html",{'services':services})

def contact_us(request):
    services=ServiceType.objects.all()
    return render(request,"src/contact_us.html",{'services':services})

def login(request):
    return render(request,"account/login.html")

def error_404(request):
    data={}
    return render(request,"src/404.html",data)