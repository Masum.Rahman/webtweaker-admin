from django.contrib.auth.models import User
from django.contrib.sites.models import Site as DjangoSite
from django.db import models

# Create your models here.
from django.db.models import DateField
from django.views.generic.dates import DayMixin

from admin_site.choices import *


class RepairShop(models.Model):
    name=models.CharField(max_length=60,default="")
    facebook=models.URLField(blank=True)
    instagram=models.URLField(blank=True)
    twitter=models.URLField(blank=True)
    google=models.URLField(blank=True)
    email=models.EmailField(blank=False,null=False)
    phone=models.CharField(max_length=15,blank=False,null=False)
    address=models.CharField(max_length=200)


class OpenClose(models.Model):
    day = models.CharField(max_length=20, blank=False, null=False, unique=True)
    from_time = models.TimeField(auto_created=True, blank=True, null=True)
    to_time = models.TimeField(auto_created=True, blank=True, null=True)
    closed=models.BooleanField(default=False)

    def __str__(self):
        return self.day
    # shop=models.ForeignKey(RepairShop)

class Review(models.Model):
    name=models.CharField(max_length=60)
    address=models.CharField(max_length=200)
    day=models.DateField(auto_now_add=True)
    message=models.TextField(max_length=2000)

#
class WebtweakerSites(DjangoSite):
    users=models.ManyToManyField(User)