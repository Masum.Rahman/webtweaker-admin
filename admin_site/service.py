from django.contrib.auth.decorators import user_passes_test, login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpRequest
from django.shortcuts import render, get_object_or_404, redirect

from admin_site.forms import ServiceTypeForm
from src.forms import ServiceOrderForm
from src.models import ServiceOrder, ServiceType

@login_required
def service_type_list(request):
    queryset=ServiceType.objects.all()
    page = request.GET.get('page')
    paginator = Paginator(queryset, 10)

    try:
        queryset = paginator.page(page)

    except PageNotAnInteger:
        queryset = paginator.page(1)

    except EmptyPage:
        queryset = paginator.page(paginator.num_pages)

    return render(request,"admin_site/service_type/service_type_list.html",{'objects':queryset})

@login_required
def service_type_create(request):
    if request.method=='POST':
        form = ServiceTypeForm(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect("admin_site:service_type_list")
    else:
        form = ServiceTypeForm()

    return render(request,"admin_site/service_type/service_type_create.html",{'form':form})

@login_required
def service_type_edit(request,id):
    service_type=get_object_or_404(ServiceType,id=id)
    form=ServiceTypeForm(request.POST or None, instance=service_type)

    if form.is_valid():
        form.save()
        return redirect('admin_site:service_type_list')

    return render(request,"admin_site/service_type/service_type_create.html",{'form':form})

@login_required
def service_type_delete(request,id):
    service=get_object_or_404(ServiceType,id=id)
    service.delete()
    return redirect("admin_site:service_type_list")


# Service Order
@login_required
def service_order_list(request):
    service_orders=ServiceOrder.objects.all()

    page = request.GET.get('page')
    paginator = Paginator(service_orders, 10)

    try:
        service_orders = paginator.page(page)

    except PageNotAnInteger:
        service_orders = paginator.page(1)

    except EmptyPage:
        service_orders = paginator.page(paginator.num_pages)

    return render(request,"admin_site/service_order/service_order_list.html",{'objects':service_orders})

@login_required
def service_order_create(request):
    if request.method=='POST':
        form = ServiceOrderForm(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect("admin_site:service_order_list")
    else:
        form = ServiceOrderForm()

    return render(request,"admin_site/service_order/service_order_create.html",{'form':form})

@login_required
def service_order_edit(request,id):
    service_order=get_object_or_404(ServiceOrder,id=id)
    form=ServiceOrderForm(request.POST or None, instance=service_order)

    if form.is_valid():
        form.save()
        return redirect('admin_site:service_order_list')

    return render(request,"admin_site/service_order/service_order_create.html",{'form':form})

@login_required
def service_order_delete(request,id):
    service = get_object_or_404(ServiceOrder, id=id)
    service.delete()
    return redirect("admin_site:service_order_list")