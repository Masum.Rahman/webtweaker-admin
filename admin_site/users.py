from django.contrib.auth.models import User
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, redirect, get_object_or_404

from admin_site.forms import UsersForm, ProfileForm


def profile(request):
    user = get_object_or_404(User, id=request.user.id)
    form = ProfileForm(request.POST or None, instance=user)

    if form.is_valid():
        form.save()
        return redirect('admin_site:users_list')

    return render(request, "account/profile.html", {'form': form})

def users_list(request):
    users=User.objects.all()
    page = request.GET.get('page')
    paginator = Paginator(users, 10)

    try:
        users = paginator.page(page)

    except PageNotAnInteger:
        users = paginator.page(1)

    except EmptyPage:
        users = paginator.page(paginator.num_pages)

    return render(request, "admin_site/user_management/users_list.html", {'objects': users})

def user_create(request):
    if request.method=='POST':
        form = UsersForm(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect("admin_site:users_list")
    else:
        form = UsersForm()

    return render(request,"admin_site/user_management/users_create.html",{'form':form})

def user_edit(request,id):
    user = get_object_or_404(User, id=id)
    form = UsersForm(request.POST or None, instance=user)

    if form.is_valid():
        form.save()
        return redirect('admin_site:users_list')

    return render(request, "admin_site/user_management/users_create.html", {'form': form})

def user_delete(request,id):
    user=get_object_or_404(User,id=id)
    user.delete()
    return redirect("admin_site:users_list")