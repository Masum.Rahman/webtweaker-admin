from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm, Select

from admin_site.choices import days
from admin_site.models import RepairShop, OpenClose
from src.models import ServiceType

class ServiceTypeForm(forms.ModelForm):

    class Meta:
        model=ServiceType
        fields=['name','description']

class UsersForm(forms.ModelForm):
    password=forms.CharField(widget=forms.PasswordInput())
    confirm_password=forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model=User
        fields=['username','email','password','confirm_password','first_name','last_name','is_active','is_superuser']
        labels={
            'is_active':'Admin',
            'is_superuser':'Super Admin'
        }

    def __init__(self,*args,**kwargs):
        super(UsersForm,self).__init__(*args,**kwargs)
        self.fields['email'].required = True

    def clean(self):
        cleaned_data=super(UsersForm,self).clean()
        password=cleaned_data.get("password")
        confirm_password=cleaned_data.get("confirm_password")

        if password!=confirm_password:
            raise forms.ValidationError(
                "Password and confirm password does not match"
            )

class ProfileForm(forms.ModelForm):

    class Meta:
        model=User
        fields=['username','first_name','last_name']

class ShopOpenCloseForm(forms.ModelForm):
    day=forms.ChoiceField(choices=days,widget=forms.Select(attrs={'class':'mdb-select'}))

    class Meta:
        model=OpenClose
        fields=['day','from_time','to_time','closed']
        widgets={
            'from_time':forms.TimeInput(attrs={'class':'timepicker form-control'}),
            'to_time':forms.TimeInput(attrs={'class':'timepicker form-control'}),
        }
        labels={
            'from_time':'From',
            'to_time':'To'
        }

class RepairShopUpdateForm(forms.ModelForm):
    class Meta:
        model=RepairShop
        fields=['name','email','address','phone','facebook','instagram','twitter','google']
